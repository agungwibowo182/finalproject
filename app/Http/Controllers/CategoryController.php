<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        $categories = Category::get();

        return view('category.index', compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->save();

        return redirect('/kategori');
    }

    public function show($id)
    {
        $category = Category::find($id);
        return view('category.show', compact('category'));
    }
    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit', compact('category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->update();
        return redirect('/kategori');
    }

    public function destroy($id)
    {
        $category  = category::find($id);
        $category->delete();
        return redirect('/kategori');
    }
}
