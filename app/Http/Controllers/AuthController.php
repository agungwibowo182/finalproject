<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function profile()
    {
        $user = Auth::user();
        $profile = Profile::all();
        return view('page.profile', compact('profile'));
    }

    public function show($id)
    {
        $profile = Profile::find($id);
        return view('page.show', compact('profile'));
    }

    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('page.edit', compact('profile'));
    }
    public function update($id, Request $request)
    {

        $profile = Profile::find($id);
        $profile->user->name = $request->name;
        $profile->user->email = $request->email;
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;

        $profile->update();
        return redirect('/');
    }

    public function destroy($id)
    {
        $profile = Profile::find($id);
        $profile->delete();
        return redirect('/');
    }
}
