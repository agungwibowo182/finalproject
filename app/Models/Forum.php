<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    use HasFactory;

    protected $table = "forum";
    protected $fillable = ["title", "content", "category"];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function users()
    {
        /**
         *params :
         * nama tabel pivot
         * nama kolom di tabel pivot yang mereferensi ke model/tabel ini
         * nama kolom di tabel pivot yang mereferensi ke model/tabel relasi
         */

        return $this->belongsToMany(User::class, 'comments', 'forum_id', 'user_id')->withPivot('komentar')->withTimestamps();
    }
}
