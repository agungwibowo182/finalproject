<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\CategoryController;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

route => mengatur jalur
view => tampilan dengan ext .blade.php 
controller => function belakang layar PHP
model => Database


*/

Route::get('/', [IndexController::class, 'utama'])->name('home');
Route::get('dashboard', [IndexController::class, 'dashboard'])->name('home');

//forum
Route::get('/forum', [ForumController::class, 'index']);
Route::get('/forum/create', [ForumController::class, 'create'])->middleware('auth');
Route::post('/forum', [ForumController::class, 'store'])->middleware('auth');
Route::get('/forum/{forum_id}', [ForumController::class, 'show']);
Route::get('/forum/{forum_id}/edit', [ForumController::class, 'edit'])->middleware('auth');
Route::put('/forum/{forum_id}', [ForumController::class, 'update'])->middleware('auth');
Route::delete('/forum/{forum_id}', [ForumController::class, 'destroy'])->middleware('auth');
Route::post('/forum/{id}/comments', [ForumController::class, 'createComments'])->middleware('auth');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [AuthController::class, 'profile'])->middleware('auth');
Route::post('/profile', [AuthController::class, 'store'])->middleware('auth');
Route::get('/profile/{id}', [AuthController::class, 'show']);
Route::get('/profile/{id}/edit', [AuthController::class, 'edit'])->middleware('auth');
Route::put('/profile/{id}', [AuthController::class, 'update'])->middleware('auth');
Route::delete('/profile/{id}', [AuthController::class, 'destroy'])->middleware('auth');
Route::post('/profile/{id}/comments', [AuthController::class, 'createComments'])->middleware('auth');

//Category
Route::get('/kategori', [CategoryController::class, 'index']);
Route::get('/kategori/create', [CategoryController::class, 'create'])->middleware('auth');
Route::get('/kategori/{id}', [CategoryController::class, 'show']);
Route::post('/kategori', [CategoryController::class, 'store'])->middleware('auth');
Route::get('/kategori/{kategori_id}/edit', [CategoryController::class, 'edit'])->middleware('auth');
Route::put('/kategori/{kategori_id}', [CategoryController::class, 'update'])->middleware('auth');
Route::delete('/kategori/{kategori_id}', [CategoryController::class, 'destroy'])->middleware('auth');
