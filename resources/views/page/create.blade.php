@extends('layouts.master')

@section('judul', 'Tambah Data')

@section('content')
        <form action="/profile" method="POST">
            @csrf
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="string" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="string" class="form-control" name="bio" id="bio" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="string" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/kategori" class="btn btn-warning">Kembali</a>
        </form>
@endsection