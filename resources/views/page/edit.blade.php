@extends('layouts.master')

@section('title','Halaman Edit Profile')

@section('content')
<a href="{{url()->previous()}}" class="btn btn-secondary mb-3">Back</a>
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="name">name</label>
        <input type="text" class="form-control" name="name" id="name" value="{{$profile->user->name}}" placeholder="Masukkan name">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" id="email" value="{{$profile->user->email}}" placeholder="Masukkan email">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" value="{{$profile->umur}}" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukkan bio">{{$profile->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Umur</label>
        <input type="text" class="form-control" name="alamat" id="alamat" value="{{$profile->alamat}}" placeholder="Masukkan alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection
