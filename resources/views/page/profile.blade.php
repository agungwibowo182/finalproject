@extends('layouts.master')

@section('title','Halaman Profile')

@section('content')

  <table id="dataTable_forum" class="table table-bordered table-striped">    
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Alamat</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($profile as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->user->name}}</td>
                <td>{{$value->user->email}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>{{$value->alamat}}</td>
                <td>
                    <a href="/profile/{{$value->id}}" class="btn btn-info">Show</a>
                    @auth
                    <a href="/profile/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/profile/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Apakah yakin ingin hapus data ?')" value="Delete">
                    </form>
                    @endauth
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection
