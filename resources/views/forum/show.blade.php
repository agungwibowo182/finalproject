@extends('layouts.master')
@section('title')
Forum Diskusi
@endsection

@section('content')

<h4>{{$forum->title}}</h4>
<p>{!!$forum->content!!}</p>

{{-- @dd($forum->users) --}}

<div class="card direct-chat direct-chat-primary">
    <div class="card-header">
      <h3 class="card-title">List Komentar</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <!-- Conversations are loaded here -->
      <div class="direct-chat-messages">
        <!-- Message. Default to the left -->
      
        @forelse ($forum->users as $key => $comment )

        @if ($key % 2  != 0)
        <div class="direct-chat-msg right">
            <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-right">{{$comment->name}}</span>
                <span class="direct-chat-timestamp float-left">{{$comment->pivot->created_at}}</span>
            </div>
            <!-- /.direct-chat-infos -->
            <img class="direct-chat-img" src="{{asset('admin/dist/img/user1-128x128.jpg')}}" alt="message user image">
            <!-- /.direct-chat-img -->
            <div class="direct-chat-text">
                {{$comment->pivot->komentar}}
            </div>
            <!-- /.direct-chat-text -->
        </div>
        @else
        <div class="direct-chat-msg">
            <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-left">{{$comment->name}}</span>
                <span class="direct-chat-timestamp float-right">{{$comment->pivot->created_at}}</span>
            </div>
            <!-- /.direct-chat-infos -->
            <img class="direct-chat-img" src="{{asset('admin/dist/img/user1-128x128.jpg')}}" alt="message user image">
            <!-- /.direct-chat-img -->
            <div class="direct-chat-text">
                {{$comment->pivot->komentar}}
            </div>
            <!-- /.direct-chat-text -->
        </div>
          <!-- /.direct-chat-msg -->

        @endif
        
        @empty
            <div>Belum memiliki Komentar</div>
        @endforelse
        <!-- /.direct-chat-msg -->
      </div>
      <!--/.direct-chat-messages-->

      <!-- Contacts are loaded here -->
      <div class="direct-chat-contacts">
        <ul class="contacts-list">
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  Count Dracula
                  <small class="contacts-list-date float-right">2/28/2015</small>
                </span>
                <span class="contacts-list-msg">How have you been? I was...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  Sarah Doe
                  <small class="contacts-list-date float-right">2/23/2015</small>
                </span>
                <span class="contacts-list-msg">I will be waiting for...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  Nadia Jolie
                  <small class="contacts-list-date float-right">2/20/2015</small>
                </span>
                <span class="contacts-list-msg">I'll call you back at...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  Nora S. Vans
                  <small class="contacts-list-date float-right">2/10/2015</small>
                </span>
                <span class="contacts-list-msg">Where is your new...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  John K.
                  <small class="contacts-list-date float-right">1/27/2015</small>
                </span>
                <span class="contacts-list-msg">Can I take a look at...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
          <li>
            <a href="#">
              <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Avatar">

              <div class="contacts-list-info">
                <span class="contacts-list-name">
                  Kenneth M.
                  <small class="contacts-list-date float-right">1/4/2015</small>
                </span>
                <span class="contacts-list-msg">Never mind I found...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
        </ul>
        <!-- /.contacts-list -->
      </div>
      <!-- /.direct-chat-pane -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <form action="/forum/{{$forum->id}}/comments" method="post">
        @csrf
        <div class="input-group">
          <input type="text" name="komentar" placeholder="Type Message ..." class="form-control">
          <span class="input-group-append">
            <input type="submit" class="btn btn-primary" value="Send">
          </span>
        </div>
      </form>
      
    </div>
    <!-- /.card-footer-->
  </div>

  <div class="row">
    <div class="col-md-12">
      <a href="/forum" class="btn btn-warning mb-3">Kembali</a>
    </div>
  </div>
@endsection