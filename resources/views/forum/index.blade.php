@extends('layouts.master')

@section('title','Halaman Forum')

@push('script')

<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dataTable_forum").DataTable();
  });
</script>
@endpush

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>    
@endpush

@section('judul')
List Forum
@endsection

@section('content')

@auth
<a href="/forum/create" class="btn btn-primary mb-3">Tambah Data</a>
@endauth

        <table id="dataTable_forum" class="table table-bordered table-striped">    
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Content</th>
                <th scope="col">Category</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($forum as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{!!$value->content!!}</td>
                        <td>{{$value->category ? $value->category->name : '-'}}</td>
                        <td>
                            <a href="/forum/{{$value->id}}" class="btn btn-info">Show</a>
                            @auth
                            <a href="/forum/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/forum/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Apakah yakin ingin hapus data ?')" value="Delete">
                            </form>
                            @endauth
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection