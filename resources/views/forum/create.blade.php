@extends('layouts.master')

@section('title', 'Tambah Data')

@push('script')
<link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.min.css')}}">
<script>
    $(function () {
      // Summernote
      $('#summernote').summernote()
  
      // CodeMirror
      CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
        mode: "htmlmixed",
        theme: "monokai"
      });
    })
  </script>
@endpush

@push('script')
<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>    
@endpush
@section('content')
        <form action="/forum" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="string" class="form-control" name="title" id="title" placeholder="Masukkan title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" value="{{old('title')}}" id="summernote" placeholder="Masukkan Content"></textarea>
                @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Select</label>
                <select class="form-control" name="category">
                    @foreach ($categories as $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/forum" class="btn btn-warning">Kembali</a>
        </form>
@endsection