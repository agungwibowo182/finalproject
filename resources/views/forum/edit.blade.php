@extends('layouts.master')
@section('title')
Edit Forum Diskusi
@endsection

@push('script')
<link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.min.css')}}">
<script>
    $(function () {
      // Summernote
      $('#summernote').summernote()
  
      // CodeMirror
      CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
        mode: "htmlmixed",
        theme: "monokai"
      });
    })
  </script>
@endpush

@push('script')
<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>    
@endpush

@section('content')

<div>
    <form action="/forum/{{$forum->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="string" class="form-control" name="title" value="{{$forum->title}}" id="title" placeholder="Masukkan Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <input type="text" class="form-control" name="content"  value="{{$forum->content}}"  id="content" placeholder="Masukkan content">
            {{-- <textarea class="form-control" name="content" value="{{old('title')}}" id="summernote" placeholder="Masukkan Content"></textarea> --}}
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            {{-- <label for="category">Category</label> --}}
            {{-- <input type="text" class="form-control" name="category"  value="{{$forum->category->name}}"  id="category" placeholder="Masukkan category"> --}}
            <label for="category">Category</label>
                <select class="form-control" name="category">
                    @foreach ($categories as $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            @error('category')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/forum" class="btn btn-warning">Kembali</a>
    </form>
</div>

@endsection