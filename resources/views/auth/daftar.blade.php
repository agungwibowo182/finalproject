<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/login-style.css')}}">
    <title>Login</title>
</head>
<body>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="{{asset('images/login.png')}}" id="icon" alt="User Icon" style="width:50px;height:50px;"/>
    </div>

    <!-- Login Form -->
    <form>
      
      <input type="text" id="name" class="fadeIn second" name="name" placeholder="Nama Lengkap">
        <input type="text" id="email" class="fadeIn second" name="email" placeholder="Email Address">
        <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">
        <input type="text" id="conformPassword" class="fadeIn third" name="confirmPassword" placeholder="confirm password">
        <input type="submit" class="fadeIn fourth" value="Daftar">
    </form>

   

  </div>
</div>  
</body>
</html>