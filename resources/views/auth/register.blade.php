<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/login-style.css')}}">
    <title>Register</title>
</head>
<body>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first m-3">
      <img src="{{asset('images/register.png')}}" id="icon" alt="User Icon" style="width:50px;height:50px;"/>
    </div>

    <!-- Login Form -->
    <form method="POST" action="{{ route('register') }}">
      @csrf
      <input id="name" type="text" class="fadeIn first @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Ketikkan nama">

      @error('name')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror

      <input id="email" type="text" class="fadeIn second @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ketikkan email">
      @error('email')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
      {{-- <input type="text" id="login" class="fadeIn second @error('email') is-invalid @enderror"  name="email" placeholder="Email Address" required autocomplete="email"> --}}
      <input id="password" type="text" class="fadeIn third @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="current-password" placeholder="password">

      @error('password')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
      <input id="password-confirm" type="text" class="fadeIn fourth" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password">
      {{-- <input type="text" id="password" class="fadeIn third" name="login" placeholder="password"> --}}
      <input type="submit" class="fadeIn fourth" value="Register">
      
    </form>

  </div>
</div>  
</body>
</html>