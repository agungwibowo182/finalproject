<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="/profile" class="d-block">{{auth()->user()? auth()->user()->name : 'guest'}}</a>
    </div>
  </div>

  <!-- SidebarSearch Form -->
  <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-item">
          <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
              Dashboard
              </p>
          </a>
      </li> 
      <li class="nav-item">
        <a href="/kategori" class="nav-link">
            <i class="nav-icon fas fas fa-th"></i>
            <p>
            Category
            </p>
        </a>
      </li> 
      <li class="nav-item">
        <a href="/forum" class="nav-link">
            <i class="nav-icon fas fas fa fa-users"></i>
            <p>
            Forum Diskusi
            </p>
        </a>
      </li> 
      <li class="nav-item">
        <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user-circle"></i>
            <p>
            Profile
            </p>
        </a>
      </li> 
      <br> 
    <?php if (auth()->user()) { ?>
      <li class="nav-item">
        <form action="/logout" method="POST">
          @csrf
          <input type="submit" class="btn btn-block btn-danger" onclick="return confirm('Apakah anda ingin logout ?')" value="Logout">
      </form>
      </li>
    <?php } else { ?>
      <li class="nav-item">
        <a href="/login" class="btn btn-block btn-primary">Login</a>
      </li>
    <?php } ?>
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>