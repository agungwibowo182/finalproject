@extends('layouts.master')
@section('title','Category')

@push('script')

<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dataTable_category").DataTable();
  });
</script>
@endpush

@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>    
@endpush

@section('judul')
List Data Category
@endsection

@section('content')

@auth
{{-- <a href="/forum/create" class="btn btn-primary mb-3">Tambah Forum</a> --}}
@endauth
<a href="/kategori/create" class="btn btn-primary mb-3">Tambah Data</a>
        <table id="dataTable_category" class="table table-bordered table-striped">    
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($categories as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>
                          <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                          <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                          <form action="/kategori/{{$value->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <input type="submit" class="btn btn-danger my-1" onclick="return confirm('Apakah anda yakin menghapus data ?')" value="Delete">
                          </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection