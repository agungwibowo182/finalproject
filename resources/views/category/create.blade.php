@extends('layouts.master')

@section('judul', 'Tambah Data')

@section('content')
        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Category</label>
                <input type="string" class="form-control" name="name" id="name" placeholder="Masukkan Category">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/kategori" class="btn btn-warning">Kembali</a>
        </form>
@endsection