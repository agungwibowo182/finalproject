@extends('layouts.master')

@section('title','Edit Category')

@section('content') 
<div>
    <form action="/kategori/{{$category->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Category</label>
            <input type="text" class="form-control" name="name" value="{{$category->name}}" id="name" placeholder="Masukkan Category">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/kategori" class="btn btn-warning">Kembali</a>
    </form>
</div>
@endsection

