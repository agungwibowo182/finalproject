@extends('layouts.master')
@section('title','List Category')

@section('content')

<h3>List Postingan dari kategori : </h3>

<div class="row"> 
@forelse ($category->forums as $forum)
<div class="col-md-3">
    <div class="card card-primary collapsed-card">
      <div class="card-header">
        <h3 class="card-title">{{$forum->title}}</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
          </button>
        </div>
        <!-- /.card-tools -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{$forum->content}}
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div> 
@empty
    Tidak ada data postingan untuk kategori ini
@endforelse
</div>

<div class="row">
  <div class="col-md-3">
    <a href="/kategori" class="btn btn-warning mb-3">Kembali</a>
  </div>
</div>


@endsection